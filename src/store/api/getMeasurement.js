import 'isomorphic-fetch'

const getMeasurement = async (metric) => {
  const query = `
    {
      getLastKnownMeasurement(metricName: "${metric}"){
        value
        unit
        at
      }
    }
  `
  const call = {
    method: "POST",
    headers: { "Content-Type": "application/json"},
    body: JSON.stringify({ query })
  }
  const response = await fetch('https://react.eogresources.com/graphql', call)
  return response.json()
}

export default getMeasurement
