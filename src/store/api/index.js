import getMetrics from './getMetrics'
import getMeasurement from './getMeasurement'
import getHistorical from './getHistorical'
export default {
  getMetrics,
  getMeasurement,
  getHistorical
};
