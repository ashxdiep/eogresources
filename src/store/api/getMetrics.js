import 'isomorphic-fetch';

const getMetrics = async () => {
  const call = {
    method: "POST",
    headers: { "Content-Type": "application/json"},
    body: JSON.stringify({ query: '{ getMetrics }' })
  }
  const response = await fetch('https://react.eogresources.com/graphql', call)
  return response.json()

}

export default getMetrics
