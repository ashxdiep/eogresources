import 'isomorphic-fetch'

const getHistorical = async (metric) => {
  const thirtyAgo = new Date();
  thirtyAgo.setMinutes(thirtyAgo.getMinutes() - 30)
  const after = Math.round(thirtyAgo.getTime() / 1000)
  const query = `
    {
      getMeasurements(input: {metricName: "${metric}", after: ${after}}) {
        value
        at
      }
    }
  `
  const call = {
    method: "POST",
    headers: { "Content-Type": "application/json"},
    body: JSON.stringify({ query })
  }
  const response = await fetch('https://react.eogresources.com/graphql', call)
  return response.json()
}

export default getHistorical
