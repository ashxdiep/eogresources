export const API_ERROR = "EVENT/API_ERROR_RECEIVED"
export const WEATHER_DATA_RECEIVED = "EVENT/WEATHER_DATA_RECEIVED"
export const STORE_METRICS_SELECTED = "SOTRE_METRICS_SELECTED"
export const GET_MEASUREMENT = "GET_MEASUREMENT"
export const FETCH_MEASUREMENT_SUCCESS = "FETCH_MEASUREMENT_SUCCESS"
export const GET_HISTORICAL = "GET_HISTORICAL"
export const FETCH_HISTORICAL_SUCCESS = "FETCH_HISTORICAL_SUCCESS"
export const STORE_CURRENT_BUTTON = "STORE_CURRENT_BUTTON"

export function storeMetrics(metrics) {
  return {
    type: STORE_METRICS_SELECTED,
    metrics
  }
}

export function getMeasurement(metric) {
  return {
    type: GET_MEASUREMENT,
    metric
  }
}

export function getHistorical(metric) {
  return {
    type: GET_HISTORICAL,
    metric
  }
}

export function saveCurrentButton(metric) {
   return {
     type: STORE_CURRENT_BUTTON,
     metric
   }
 }
