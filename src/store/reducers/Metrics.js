import * as types from "../actions"

const initialState = {
  selectedMetrics : [],
  currentButton: '',
  tubingPressure: {
    current: 0,
    unit: '',
    history: []
  },
  casingPressure: {
    current: 0,
    unit: '',
    history: []
  },
  oilTemp: {
    current: 0,
    unit: '',
    history: []
  },
  flareTemp: {
    current: 0,
    unit: '',
    history: []
  },
  waterTemp: {
    current: 0,
    unit: '',
    history: []
  },
  injValveOpen: {
    current: 0,
    unit: '',
    history: []
  }

}

export default function (state = initialState, action) {
  switch(action.type) {
    case types.STORE_METRICS_SELECTED:
      return {
        ...state,
        selectedMetrics: action.metrics
      }
    case types.STORE_CURRENT_BUTTON:
      return {
        ...state,
        currentButton: action.metric
      }
    case types.FETCH_MEASUREMENT_SUCCESS:
      const metric  = {
        ...state[action.metric],
        current: action.measurement.value,
        unit: action.measurement.unit
      }
      return {
        ...state,
        [action.metric]: metric
      }
    case types.FETCH_HISTORICAL_SUCCESS:
      const historical = {
        ...state[action.metric],
        history: action.history
      }
      return {
        ...state,
        [action.metric]: historical
      }
    default:
      return state
  }
}
