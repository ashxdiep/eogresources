import { createStore, applyMiddleware, combineReducers } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import createSagaMiddleware from "redux-saga";
import sagas from "./sagas";
import metricReducer from "./reducers/Metrics";

export default () => {
  const rootReducer = combineReducers({
    metrics: metricReducer
  });

  const composeEnhancers = composeWithDevTools({});
  const sagaMiddleware = createSagaMiddleware();
  const middlewares = applyMiddleware(sagaMiddleware);
  const store = {
    ...createStore(rootReducer, composeEnhancers(middlewares)),
    runSaga: sagaMiddleware.run(sagas)
  }

  return store;
};
