import {
  fork,
  takeEvery
} from 'redux-saga/effects';
import * as types from '../actions'
/* Sagas*/
import { fetchMeasurementSaga, fetchHistoricalSaga } from './Metrics'
import { apiErrorReceived } from './ApiErrors'

function* watchMetrics() {
  yield takeEvery(types.GET_HISTORICAL, fetchHistoricalSaga)
  yield takeEvery(types.GET_MEASUREMENT, fetchMeasurementSaga)
}

function* watchApiError() {
  yield takeEvery(types.API_ERROR, apiErrorReceived);
}

export default function* startForman() {
  yield fork(watchMetrics)
  yield fork(watchApiError)
}
