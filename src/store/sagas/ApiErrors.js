import { call } from "redux-saga/effects";
import { toast } from "react-toastify";

export function* apiErrorReceived(action) {
  yield call(toast.error, `Error Received: ${action.error}`);
}
