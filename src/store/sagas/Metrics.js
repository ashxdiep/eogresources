import { put, call } from 'redux-saga/effects'
import * as types from '../actions'
import API from '../api'
import moment from 'moment'

export function* fetchMeasurementSaga({ metric }){
  try {
    const measurement = yield call(fetchMeasurementService, metric)
    yield put({ type: types.FETCH_MEASUREMENT_SUCCESS, measurement, metric })
  } catch (error) {
    yield put ({ type: types.API_ERROR, error })
  }
}

const fetchMeasurementService = (metric) => {
  return API.getMeasurement(metric).then(response => response.data.getLastKnownMeasurement)
}

export function* fetchHistoricalSaga({ metric }) {
  try {
    const history = yield call(fetchHistoricalService, metric)
    yield put({ type: types.FETCH_HISTORICAL_SUCCESS, history, metric })
  } catch (error) {
    yield put ({ type: types.API_ERROR, error })
  }
}

const fetchHistoricalService = (metric) => {
  return API.getHistorical(metric).then(response => {
    const measurements = response.data.getMeasurements
    const converted = measurements.map(measurement => {
      let time = new Date(measurement.at * 1000)
      time = `${moment(time).format('LT')}`
      return { time, value: measurement.value }
    })
    return converted.slice(converted.length - 30)
  })
}
