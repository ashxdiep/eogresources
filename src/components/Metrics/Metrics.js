import React from 'react'
/* Mui */
import {
  Grid,
  AppBar,
  Toolbar,
  Hidden,
  Drawer,
  Typography,
  IconButton
} from '@material-ui/core'
import {
  Menu
} from '@material-ui/icons'
import { makeStyles } from '@material-ui/core/styles'
/* Snippets */
import MetricSelect from './Snippets/MetricSelect'
import SideMenu from './Snippets/SideMenu'
import Graph from './Snippets/Graph'

const drawerWidth = 400;
const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex'
  },
  appBar: {
    marginLeft: drawerWidth,
    [theme.breakpoints.up('sm')]: {
      width: `calc(100% - ${drawerWidth}px)`,
    }
  },
  drawer: {
    [theme.breakpoints.up('sm')]: {
      width: drawerWidth,
      flexShrink: 0,
    }
  },
  graph: {
    marginLeft: drawerWidth,
    [theme.breakpoints.up('sm')]: {
      width: `calc(100% - ${drawerWidth}px)`
    }
  },
  menuButton: {
    marginRight: theme.spacing(2),
    [theme.breakpoints.up('sm')]: {
      display: 'none',
    }
  },
  fixed: {
    top: 'auto',
    left: 'auto',
    right: 0,
    position: 'fixed'
  },
  content: {
    paddingTop: '5%'
  },
  drawerPaper: {
    top: 'auto',
    width: drawerWidth,
  },
}))

function Metrics(props) {
  const { container } = props
  const classes = useStyles();
  const [mobileOpen, setMobileOpen] = React.useState(false)

  function handleDrawerToggle() {
    setMobileOpen(!mobileOpen);
  }

  return(
    <Grid container display="flex" direction="column" alignContent="center">
      <Grid item sm={12} md={4}>
        <MetricSelect />
      </Grid>

      <Grid item sm={12}>
        <div className={classes.root}>
          <AppBar className={classes.appBar} position="fixed" classes={{positionFixed: classes.fixed }}>
            <Toolbar>
              <IconButton
                color="inherit"
                aria-label="open drawer"
                edge="start"
                onClick={handleDrawerToggle}
                className={classes.menuButton}
              >
                <Menu />
              </IconButton>
              <Typography variant="h6" noWrap>
                Historial Data
              </Typography>
            </Toolbar>
          </AppBar>
          <nav className={classes.drawer}>
            <Hidden smDown implementation="css">
              <Drawer
                container={container}
                classes={{paper: classes.drawerPaper}}
                variant="temporary"
                open={mobileOpen}
                onClose={handleDrawerToggle}
                ModalProps={{
                  keepMounted: true
                }}
              >
               <SideMenu />
              </Drawer>
            </Hidden>

            <Hidden smDown implementation="css">
              <Drawer
                classes={{paper: classes.drawerPaper}}
                variant="permanent"
                open
              >
                <SideMenu />
              </Drawer>
            </Hidden>
          </nav>
          <main className={classes.content}>
            <div className={classes.toolbar} />
            <Graph />
          </main>
        </div>
      </Grid>
    </Grid>
  )
}

export default Metrics
