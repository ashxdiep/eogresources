import React, { useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { getMeasurement, getHistorical, saveCurrentButton } from '../../../store/actions'
/* Mui */
import {
  List,
  ListItem,
  ListItemText,
  Divider,
  Typography
} from '@material-ui/core'
/* Utils */
import useInterval from '../../Utils/UseInterval'

const MetricButton = (props) => {
  const { metric } = props
  const dispatch = useDispatch()
  const measurement = useSelector(state => state.metrics[metric].current)
  const unit = useSelector(state => state.metrics[metric].unit)

  useEffect( () => { dispatch(getHistorical(metric) )}, [dispatch, metric])
  useInterval(() => {
    dispatch(getMeasurement(metric))
  } , 1000)

  return (
    <ListItem button onClick={() => dispatch(saveCurrentButton(metric))}>
      <ListItemText primary={metric} secondary={`Value: ${measurement} ${unit}`} />
    </ListItem>
  )
}

function SideMenu() {
  const metrics = useSelector(state => state.metrics.selectedMetrics)
  return(
    <div>
    <Typography style={{padding: '12px 0 '}} variant="h5" align="center"> Click to View Analysis</Typography>
      <Divider />
      <List>
        {metrics.map((metric, i) => (
          <MetricButton key={i} metric={metric} />
        ))}
      </List>
    </div>
  )
}

export default SideMenu
