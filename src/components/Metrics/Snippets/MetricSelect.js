import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { storeMetrics, saveCurrentButton } from '../../../store/actions'
/* API */
import API from '../../../store/api'
/* Mui */
import {
  FormControl,
  InputLabel,
  Select,
  Chip,
  MenuItem,
  Input
} from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles(theme => ({
  form: {
    width: '100%',
  },
  input: {
    margin: '20px 0'
  }
}))

function MetricSelect() {
  const dispatch = useDispatch()
  const classes = useStyles()
  const current = useSelector(state => state.metrics.currentButton)
  const [metrics, setMetrics] = React.useState([])
  const [metricName, setMetricName] = React.useState([])

  useEffect( () => { fetchMetrics() }, [])

  const fetchMetrics = () => {
    API.getMetrics().then(res => {
      const list = res.data.getMetrics;
      setMetrics(list)
    })
  }

  function handleChange(event) {
    setMetricName(event.target.value)
    dispatch(storeMetrics(event.target.value))
    if (!event.target.value.includes(current)){
      dispatch(saveCurrentButton(''))
    }
  }

  return(
    <FormControl className={classes.form}>
      <InputLabel htmlFor="select-metrics">Select Metrics</InputLabel>
      <Select
        multiple
        value={metricName}
        onChange={handleChange}
        input={<Input classes={{formControl: classes.input}}  id="select-metrics" />}
        renderValue={selected => (
          <div>
            {selected.map(value => (
              <Chip key={value} label={value} />
            ))}
          </div>
        )}
      >
        {metrics.map(name => (
          <MenuItem key={name} value={name}>{name}</MenuItem>
        ))}
      </Select>
    </FormControl>
  )
}

export default MetricSelect
