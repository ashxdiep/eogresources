import React from 'react'
import { useSelector } from 'react-redux'
/* Mui */
import {
  Typography
} from '@material-ui/core'
/* Recharts */
import {
  LineChart,
  Line,
  XAxis,
  YAxis,
  Tooltip,
  Legend,
  CartesianGrid
} from 'recharts'

function Graph() {
  const current = useSelector(state => state.metrics.currentButton)
  const metric = () => {
    if (current.length !== 0) return current
    else return 'oilTemp'
  }
  const list = useSelector(state => state.metrics.selectedMetrics)
  const data = useSelector(state => state.metrics[metric()].history)
  const unit = useSelector(state => state.metrics[metric()].unit)

  return (
    <div>
      {current.length !== 0 && list.includes(current) ?
        <>
          <Typography variant="h2" align="center" style={{marginTop: '10%'}}> {current} </Typography>
          <LineChart
            width={500}
            height={500}
            data={data}
            margin={{top:100, bottom: 5}}
          >
            <CartesianGrid strokeDasharray="3 3" />
            <XAxis dataKey="time" />
            <YAxis />
            <Tooltip />
            <Legend />
            <Line type="monotone" name={`${current} in ${unit}`} dataKey="value" stroke="#8884d8" />
          </LineChart>
        </>: null
      }
    </div>
  )
}

export default Graph
